$(function() {
    $(document).on('click', '.js-toggle', function(evt) {
        evt.preventDefault();
        var $this = $(this);
        var $target = $($this.data('toggle-target'));
        $target.fadeToggle();
    });
    $('body').css({'padding-top': $('.header').innerHeight() });
});
